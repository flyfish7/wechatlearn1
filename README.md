# wechatlearn1


#### 软件架构
┌─components            uni-app组件目录
│  └─comp-a.vue         可复用的a组件
├─pages                 业务页面文件存放的目录
│  ├─index
│  │  └─index.vue       index页面
│  └─list
│     └─list.vue        list页面
├─static                存放应用引用静态资源（如图片、视频等）的目录，注意：静态资源只能存放于此
├─main.js               Vue初始化入口文件
├─App.vue               应用配置，用来配置小程序的全局样式、生命周期函数等
├─manifest.json         配置应用名称、appid、logo、版本等打包信息
└─pages.json            配置页面路径、页面窗口样式、tabBar、navigationBar 等页面类信息

微信小程序：使用微信开发者工具和Hbuilder开发
黑马优购接口文档:https://applet-base-api-t.itheima.net/
微信开发文档API:https://developers.weixin.qq.com/miniprogram/dev/api/
uni-app 官方文档，请翻阅 https://uniapp.dcloud.net.cn/



#### 环境 说明
1.  使用hbuilder打开该项目
2.  npm install
2.1 相关的依赖在package-lock.json  
3.  运行-运行到微信小程序模拟器

#### 使用说明

1.  使用hbuilder打开该项目
1.1 安装 scss/sass 编译
为了方便编写样式（例如：<style lang="scss"></style>），建议安装 scss/sass 编译 插件。插件下载地址：
https://ext.dcloud.net.cn/plugin?name=compile-node-sass
进入插件下载页面之后，点击右上角的 使用 HBuilderX 导入插件 按钮进行自动安装，截图如下：
2.  npm install
3.  运行-运行到微信小程序模拟器
